package us.skyguardian.driverns6;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class ViajeActivity extends AppCompatActivity {

    String url= "http://192.169.213.91:9001/apps/pagogas/login.php";

    ProgressDialog progress;
    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viaje);

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://192.169.213.91:9001/apps/pagogas/selectunit.php?user=tecnicaronaldo&access_token=355d2d1476b4e2d1c3c86aa1fa6db20b02F3730BA9B1FBC6931DB3E26E6E3D60D9CD1E1F"));
        startActivity(browserIntent);

        //getSupportActionBar().hide(); //Ocultar el Toolbar de la ventana principal
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //webView = (WebView) findViewById(R.id.webViewXml);
        final WebView navegador2 = (WebView)findViewById(R.id.webViewXml);
        WebSettings webSettings = navegador2.getSettings();
        webSettings.setJavaScriptEnabled(true);
        navegador2.setWebChromeClient(new WebChromeClient());
        final Intermediario intermediario = new Intermediario(ViajeActivity.this);
        navegador2.addJavascriptInterface(intermediario, "Android");
        navegador2.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }
        });

        navegador2.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                //super.onReceivedSslError(view, handler, error);
                androidx.appcompat.app.AlertDialog.Builder builder;
                builder = new androidx.appcompat.app.AlertDialog.Builder(ViajeActivity.this);
                String message = "";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += "\"SSL Error de Certificado\" \n Deseas continuar?";
                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });

                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                androidx.appcompat.app.AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String username = extras.getString("usuario");
            String token = extras.getString("contraseña");
            //navegador2.loadUrl("http://192.169.213.91:9001/apps/pagogas/selectunit.php?user=tecnicaronaldo&access_token=355d2d1476b4e2d1c3c86aa1fa6db20b02F3730BA9B1FBC6931DB3E26E6E3D60D9CD1E1F");
            navegador2.loadUrl("http://192.169.213.91:9001/apps/pagogas/selectunit.php?user="+username+"&access_token="+token);
            download();
        }else{
            navegador2.loadUrl("https://telematics.skyguardian.mx/login.html?client_id=FuelappSkyguardian&access_type=-1&lang=es&activation_time=0&duration=0&flags=0x1&redirect_uri=http://192.169.213.91:9001/apps/pagogas/validar.php");
        }
    }
    public void download(){
        progress=new ProgressDialog(this);
        progress.setMessage("Cargando información...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(false);
        progress.setProgress(0);
        progress.setMax(100);
        progress.show();
        progress.setCancelable(false);

        final int totalProgressTime = 100;
        final Thread t = new Thread() {
            @Override
            public void run() {
                int jumpTime = 0;

                while(jumpTime < totalProgressTime) {
                    try {
                        sleep(2000);
                        jumpTime += 25;
                        progress.setProgress(jumpTime);
                    }
                    catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                progress.dismiss();
            }
        };
        t.start();
    }

    // INTERFACE JAVASCRIPT
    public class Intermediario {
        int i=0;
        Context mContext;
        //private static final int TAKE_PHOTO_CODE = 1;


        Intermediario(Context c) {
            mContext = c;
        }

        /*@android.webkit.JavascriptInterface
        public void RecibirToken(final String token, final String username){
            runOnUiThread(new Runnable() {
                public void run() {
                    SharedPreferences settings = getSharedPreferences("us.skyguardian.skygastelematics", 0);
                    SharedPreferences.Editor editor = settings.edit();

                    int num = MainActivity.cuentas.size();
                    editor.putString("us.skyguardian.skygastelematics.user"+(num+1), username);
                    editor.putString("us.skyguardian.skygastelematics.pass"+(num+1), token);

                    editor.commit();
                }
            });
        }

        @android.webkit.JavascriptInterface
        public void regresarAlogin(){
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }*/
    }
}