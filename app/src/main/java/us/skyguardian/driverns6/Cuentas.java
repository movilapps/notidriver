package us.skyguardian.driverns6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class Cuentas extends AppCompatActivity {

    Button[] botones_cuentas;
    Button[] botones_cuentasX;
    Button btn_otra_cuenta;
    public ArrayList<String[]> cuentas;
    SharedPreferences settings;
    String[] temp = new String[2];
    int finalI=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuentas);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btn_otra_cuenta = (Button) findViewById(R.id.btn_otra_cuenta);
        btn_otra_cuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(Cuentas.this,MainActivity.class);
                Bundle extras = new Bundle();
                extras.putBoolean("PERMANECER", true);
                i.putExtras(extras);
                startActivity(i);
            }
        });

        settings = getSharedPreferences("us.skyguardian.skygastelematics", 0);
        cuentas = new ArrayList<String[]>();
        botones_cuentas = new Button[3];
        botones_cuentasX = new Button[3];
        //System.out.println("MUESTRA: " + settings.toString());
        for(int i=1;i<=3;i++){
            if(settings.getString("us.skyguardian.skygastelematics.user"+i, "USER") != "USER"){
                //final String[] temp = new String[2];
                temp[0]=settings.getString("us.skyguardian.skygastelematics.user"+i, "USER");
                temp[1]=settings.getString("us.skyguardian.skygastelematics.pass"+i, "PASS");
                cuentas.add(temp);
                //System.out.println("Hola123: " + cuentas.get(i-1).toString());

                switch(i){
                    case 1:
                        botones_cuentas[i-1] = (Button) findViewById(R.id.btn_cuenta1);
                        botones_cuentasX[i-1] = (Button) findViewById(R.id.btn_cuentaX1);
                        break;
                    case 2:
                        botones_cuentas[i-1] = (Button) findViewById(R.id.btn_cuenta2);
                        botones_cuentasX[i-1] = (Button) findViewById(R.id.btn_cuentaX2);
                        break;
                    case 3:
                        botones_cuentas[i-1] = (Button) findViewById(R.id.btn_cuenta3);
                        botones_cuentasX[i-1] = (Button) findViewById(R.id.btn_cuentaX3);
                        break;
                }
                botones_cuentas[i-1].setText(temp[0]);
                botones_cuentas[i-1].setVisibility(View.VISIBLE);
                botones_cuentasX[i-1].setVisibility(View.VISIBLE);
                botones_cuentas[i-1].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        Intent i = new Intent(Cuentas.this,ViajeActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("usuario",temp[0]);
                        extras.putString("contraseña",temp[1]);
                        i.putExtras(extras);
                        startActivity(i);
                    }
                });
                finalI = i;
                botones_cuentasX[i-1].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        Aviso().show();
                    }
                });
            }
        }
    }

    public AlertDialog Aviso() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("¡AVISO!")
                .setMessage("¿Deseas borrar esta cuenta?")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                System.out.println("Hola " + (finalI-1));
                                //botones_cuentas[finalI -1].setVisibility(View.GONE);
                                botones_cuentas[finalI-1].setText("");
                                //System.out.println("MUESTRA Primeras1 : " + temp[0]);
                                //System.out.println("MUESTRA  segundas1 : " + temp[1]);
                                //System.out.println("MUESTRA1 " + temp[0]);
                                temp[0]= String.valueOf(settings.edit().remove("us.skyguardian.skygastelematics.user"+(finalI)).commit());
                                temp[1]= String.valueOf(settings.edit().remove("us.skyguardian.skygastelematics.pass"+(finalI)).commit());

                                //System.out.println("MUESTRA Primeras: " + temp[0]);
                                //System.out.println("MUESTRA  segundas: " + temp[1]);
                                //System.out.println("MUESTRA  Terceras: " + temp);

                                cuentas.remove(temp);
                                botones_cuentas[finalI -1].setVisibility(View.GONE);
                                botones_cuentasX[finalI -1].setVisibility(View.GONE);
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //listener.onNegativeButtonClick();
                            }
                        });

        return builder.create();
    }
}
