package us.skyguardian.driverns6;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.LineRadarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

public class BitacoraActivity extends AppCompatActivity {
    private static final String TAG = "BitacoraActivity";
    private LineChart lineChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitacora);

        lineChart = (LineChart) findViewById(R.id.graficaLinear);

        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(false);

        ArrayList<Entry> yValues = new ArrayList<>();
        yValues.add(new Entry(00, 5f));
        yValues.add(new Entry(1, 5f));
        yValues.add(new Entry(2, 5f));
        yValues.add(new Entry(3, 5f));
        yValues.add(new Entry(4, 5f));
        yValues.add(new Entry(5, 5f));
        yValues.add(new Entry(6, 5f));
        yValues.add(new Entry(7, 5f));
        yValues.add(new Entry(8, 5f));
        yValues.add(new Entry(9, 5f));
        yValues.add(new Entry(10, 5f));
        yValues.add(new Entry(11, 5f));
        yValues.add(new Entry(12, 5f));
        yValues.add(new Entry(13, 5f));
        yValues.add(new Entry(14, 5f));
        yValues.add(new Entry(15, 5f));
        yValues.add(new Entry(16, 5f));
        yValues.add(new Entry(17, 5));
        yValues.add(new Entry(18, 5f));
        yValues.add(new Entry(19, 6f));
        yValues.add(new Entry(20, 5f));
        yValues.add(new Entry(21, 6f));
        yValues.add(new Entry(22, 5f));
        yValues.add(new Entry(23, 5f));

        LineDataSet set1 = new LineDataSet(yValues, "Data set 1");

        set1.setFillAlpha(110);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);
        set1.setColor(Color.BLACK);
        set1.setDrawFilled(true);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        lineChart.setData(data);






    }
}