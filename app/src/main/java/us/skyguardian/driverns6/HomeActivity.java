package us.skyguardian.driverns6;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HomeActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private static String receiveParam, nombreUnidad;
    private BottomNavigationView bottomNavigationView;
    private int valVisibilidad = 0;
    private TextView origen, destino;
    /****************************** DECLARACION DE BOTONES PARA FUNCIONES *************************************************/
    private FloatingActionButton btnComida;
    private FloatingActionButton btnDiesel;
    private FloatingActionButton btnWC;
    private FloatingActionButton btnDescanso;
    private FloatingActionButton btnReparacionMecanica;
    private FloatingActionButton btnTrafico;
    private FloatingActionButton btnReten;
    private FloatingActionButton btnLlanta;
    private FloatingActionButton btnSos;
    private FloatingActionButton btnCarga;
    private FloatingActionButton btnDescarga;
    private FloatingActionButton btnContinuar;
    private ImageButton btnPausa;
    private ImageButton btnMatt;
    /*******************************************************************************/

    //private AdapterMensajes adapterMensajes;
    private FirebaseDatabase firebaseDatabase;
    private static DatabaseReference databaseReference;
    private String android_id = "", key;


    private String auxQuery = "";
    private static String auxQueryID = "";

    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int READ_PHONE_STATE = 107;

    double LongData, LatData;
    //Variables para el permiso de localización;
    private LocationManager locationManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FusedLocationProviderClient fusedLocationClient;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    LocationTrack locationTrack;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    String[] linkLocation = {""};
    String[] MsgTexto = {" Realizó parada por comida ",                                  //1
            " Realizó parada por WC ",                                                   //2
            " Realizó parada para carga de combustible ",                                //3
            " Realizó parada para descansar ",                                           //4
            " Realizó parada por mal clima ",                                            //5
            " Realizó parada por una avería en la unidad ",                              //6
            " Realizó parada por reten ",                                                //7
            " Me encuentro en tráfico carretero ",                                       //8
            " SOS enviado, atenta reacción, favor de marcar o actuar de manera veloz ",  //9
            " En recepción de mercancia ",                                               //10
            " En entrega de mercancia ",                                                 //11
            " Conduciendo ",                                                             //12
            " Fuera de servicio ",                                                       //13
            " Realizó parada por una pausa ",                                            //14
            " Realizó parada por actividad auxiliar ",                                   //15
            " Continuando con el viaje "                                                 //16
    };

    String host = "193.193.165.165"; //host y puerto de wialon
    int port = 20963;

    private String recibeVista = "";
    private String recibeUnidad = "", recibeTel = "", recibeVal = "";

    static final Integer PHONESTATS = 0x1;
    private final String TAG = HomeActivity.class.getSimpleName();
    private LinearLayout vistaActividades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        key = getIntent().getStringExtra("llave");

        String unitId = "";
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))) {
                //mostrarExplicacionStatePhone();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
            }
        } else {
            //El permiso ya fue activado
        }

        if (telephonyManager.getDeviceId() != null) {
            unitId = telephonyManager.getDeviceId();
            System.out.println("IMEI: " + unitId);
        }

        android_id = Settings.Secure.getString(HomeActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);

        //Toast.makeText(HomeActivity.this, "IMEI: " + android_id,  Toast.LENGTH_LONG).show();

        String password = "C0ntr0lCCTSky19";
        MessageSender.initWithHost(host, port, key, password);


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        /******************************* INICIALIZACION DE LOS BOTONES DE ACTIVIDADES ********************************************************/
        btnComida = findViewById(R.id.xml_imgBtnFood);
        btnWC = findViewById(R.id.xml_imgBtnWC);
        btnDiesel = findViewById(R.id.xml_imgBtnDiesel);
        btnDescanso = findViewById(R.id.xml_imgBtnDescanso);
        btnLlanta = findViewById(R.id.xml_imgBtnLlanta);
        btnReparacionMecanica = findViewById(R.id.xml_imgBtnReparacionMecanica);
        btnReten = findViewById(R.id.xml_imgBtnReten);
        btnTrafico = findViewById(R.id.xml_imgBtnTrafico);
        btnSos = findViewById(R.id.xml_btnImgSos);
        btnCarga = findViewById(R.id.xml_imgBtnCarga);
        btnDescarga = findViewById(R.id.xml_imgBtnDescarga);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_chat);
        btnContinuar = findViewById(R.id.xml_btnContinuar);
        btnPausa = findViewById(R.id.xml_imgBtnPausa);
        btnMatt = findViewById(R.id.xml_imgBtnMatt);

        bottomNavigationView.setSelectedItemId(R.id.notificaciones_bottom);


        /***************************************************************************************/

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(auxQuery); //Nombre de la sala de chat donde se guardaran todos los mensajes.

        //adapterMensajes = new AdapterMensajes(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        /*rvchat.setLayoutManager(linearLayoutManager);
        rvchat.setAdapter(adapterMensajes);*/


        /******************************* CAMBIO DE ACTIVIDADES CON EL BOTTOM NAVIGATION VIEW ********************************************************/

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.notificaciones_bottom:
                        int validaInternet = verificarSenal();
                        if (validaInternet == 1) {
                            locationTrack = new LocationTrack(HomeActivity.this);
                            if (locationTrack.canGetLocation()) {
                                double longitude = locationTrack.getLongitude();
                                double latitude = locationTrack.getLatitude();
                                LongData = longitude;
                                LatData = latitude;
                            } else {
                                locationTrack.showSettingsAlert();
                            }
                            accederPermisoLocation(MsgTexto[12], 1);
                        } else {
                            if (validaInternet == 0) {
                                Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                            }
                        }
                        break;
                    case R.id.home_bottom:
                        int validaInternet1 = verificarSenal();
                        if (validaInternet1 == 1) {
                            locationTrack = new LocationTrack(HomeActivity.this);
                            if (locationTrack.canGetLocation()) {
                                double longitude = locationTrack.getLongitude();
                                double latitude = locationTrack.getLatitude();
                                LongData = longitude;
                                LatData = latitude;
                            } else {
                                locationTrack.showSettingsAlert();
                            }
                            accederPermisoLocation(MsgTexto[11], 1);
                        } else {
                            if (validaInternet1 == 0) {
                                Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                            }
                        }
                        break;
                }
                return true;
            }
        });

        /********************************* AGREGA FUNCION DE LOS BOTONES DE ACTIVIDADES SECUNDARIOS *************************************************************/
        btnComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[0], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnWC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[1], 2);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnDiesel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[2], 3);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnDescanso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[3], 4);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnLlanta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[4], 5);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnReparacionMecanica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[5], 6);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        btnReten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[6], 7);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnTrafico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[7], 8);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[8], 9);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnCarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[9], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnDescarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[10], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnPausa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[13], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnMatt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[14], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if (validaInternet == 1) {
                    locationTrack = new LocationTrack(HomeActivity.this);
                    if (locationTrack.canGetLocation()) {
                        double longitude = locationTrack.getLongitude();
                        double latitude = locationTrack.getLatitude();
                        LongData = longitude;
                        LatData = latitude;
                    } else {
                        locationTrack.showSettingsAlert();
                    }
                    accederPermisoLocation(MsgTexto[15], 1);
                } else {
                    if (validaInternet == 0) {
                        Toast.makeText(HomeActivity.this, "Mensaje guardado, debido a perdida de conexión de Internet.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        /************************************************************************************************/
    }

    private int verificarSenal() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    private void setScroll() {
        //rvchat.scrollToPosition(adapterMensajes.getItemCount() - 1);
    }

    private void accederPermisoLocation(final String mensaje, final int validaActividad) {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }

            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            linkLocation[0] = "http://maps.google.com/maps?q=loc:" + location.getLatitude() + "," + location.getLongitude();
                            //new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + linkLocation[0]);

                            System.out.println("coordenadas enviadas a telematics: " + location.getLatitude() + ", " + location.getLongitude());
                            System.out.println("coordenadas enviadas a telematics: " + LatData + ", " + LongData);

                            new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),mensaje);

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(location.getLatitude(), location.getLongitude(), location.getAltitude(), location.getSpeed(), (short) location.getBearing(), (byte)location.getAccuracy()))
                                    .batteryLevel((byte)batLevel)
                                    .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");
                            System.out.println("mensaje gurtam: " +  message);

                            MessageSender.sendMessage(message, new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n" + mensaje, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }); //método de envío de mensaje a wialon.

                            if(validaActividad == 3){
                                Intent intent = new Intent (HomeActivity.this, MainActivity.class);
                                startActivity(intent);
                                    /*Intent intent = new Intent (ChatActivity.this, registerFuel.class);
                                    intent.putExtra("unit", recibeUnidad);
                                    intent.putExtra("x", location.getLongitude());
                                    intent.putExtra("y", location.getLatitude());
                                    intent.putExtra("parametro", "1");
                                    intent.putExtra("tellogin", recibeTel);
                                    intent.putExtra("valida", recibeVal);
                                    intent.putExtra("param", receiveParam);
                                    intent.putExtra("llave", key);
                                    //intent.putExtra("nombreUnidad", nombreUnidad);
                                    startActivity(intent);*/
                            }
                        }
                        else{
                            //new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + "Ubicación desconocida");
                            new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,"0","0",mensaje);

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);


                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .batteryLevel((byte)batLevel)
                                    .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                            if(validaActividad == 3) {
                                Intent intent = new Intent(HomeActivity.this, registerFuel.class);
                                intent.putExtra("unit", recibeUnidad);
                                intent.putExtra("x", 0);
                                intent.putExtra("y", 0);
                                intent.putExtra("parametro", "1");
                                intent.putExtra("tellogin", recibeTel);
                                intent.putExtra("valida", recibeVal);
                                intent.putExtra("param", receiveParam);
                                intent.putExtra("llave", key);
                                //intent.putExtra("nombreUnidad", nombreUnidad);
                                startActivity(intent);
                            }
                        }
                    }
                });
            }
        } else {//Si la API es menor a 23 - llamo al metodo de ubicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        //System.out.println(.*)
                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();
                        //new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + linkLocation[0]);
                        new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                        if(validaActividad == 3) {
                            Intent intent = new Intent(HomeActivity.this, registerFuel.class);
                            intent.putExtra("unit", recibeUnidad);
                            intent.putExtra("x", location.getLongitude());
                            intent.putExtra("y", location.getLatitude());
                            intent.putExtra("parametro", "1");
                            intent.putExtra("tellogin", recibeTel);
                            intent.putExtra("valida", recibeVal);
                            intent.putExtra("param", receiveParam);
                            intent.putExtra("llave", key);
                            //intent.putExtra("nombreUnidad", nombreUnidad);
                            startActivity(intent);
                        }
                    }
                    else{
                        //new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + "Ubicación desconocida");
                        new HomeActivity.registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,"0","0",mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("AVISO: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Notificación enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la notificación.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });

                        if(validaActividad == 3) {
                            Intent intent = new Intent(HomeActivity.this, registerFuel.class);
                            intent.putExtra("unit", recibeUnidad);
                            intent.putExtra("x", 0);
                            intent.putExtra("y", 0);
                            intent.putExtra("parametro", "1");
                            intent.putExtra("tellogin", recibeTel);
                            intent.putExtra("valida", recibeVal);
                            intent.putExtra("param", receiveParam);
                            intent.putExtra("llave", key);
                            //intent.putExtra("nombreUnidad", nombreUnidad);
                            startActivity(intent);
                        }
                    }
                }
            });
        }
    }

    private void mostrarExplicacion() {
        new AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la ultima y actual ubicación del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada() {
        Toast.makeText(getApplicationContext(), "Haz rechazado la petición, puede suceder que la app no trabaje de manera adecuada.", Toast.LENGTH_SHORT).show();
    }

    public class registerEvent extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("unit_x", strings[1]);
                paramObject.put("unit_y", strings[2]);
                paramObject.put("unit_txt", strings[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            client.connectTimeoutMillis();
            //verificar error, el método inicia en la linea 810
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/event")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println(.*)
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //System.out.println(.*)
            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    //Toast.makeText(getApplicationContext(), "Notificación registrada exitosamente", Toast.LENGTH_LONG).show();
                }else{
                    String msg = jo.getString("message");
                    //Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en el servidor, para registro de evento.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private void displayRoute(String origen, String destino){
        try{
            Uri uri = Uri.parse("https://www.google.co.in/maps/dir/" + origen + "/" + destino);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.setPackage("com.google.android.apps.maps");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps");
            Intent intentI = new Intent(Intent.ACTION_VIEW, uri);
            intentI.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentI);
        }
    }

    @Override
    public void onBackPressed() {
    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(HomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}